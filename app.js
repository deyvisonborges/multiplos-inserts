const express = require('express');
const commands = require('./database');
const bp = require('body-parser');

const app = express();

app.use(bp.json());
app.use(bp.urlencoded());

app.get('/', (req, res) => {
    return res.json({ 
        text: 'Welcome'
    });
});

app.post('/', async (req, res) => {
    let data = {
        codigo_pessoa: req.body.codigo_pessoa,
        nome: req.body.nome,
        cartoes: [req.body.cartoes]
    };

    if (data.cartoes) {
        let verificaInsert = await commands.verificaInsert('cartoes', req.body.codigo_pessoa);        
        if (verificaInsert == '' || verificaInsert == []) {
            for (let i = 0; i < req.body.cartoes.length; i ++) {
                data.cartoes = req.body.cartoes[i];
                await commands.insert('pessoa', data);
                console.log("Dados inseridos com sucesso!");
            }          
        } 
        else {
            for (let i = 0; i < verificaInsert.length; i++) {
                console.log(verificaInsert[i]['cartoes']);
            }
            return res.json({
                msg: "Já foram adicionados cartões a essa conta"
            });
        }
    }
    return res.json(data);
})
app.listen(3000, () => {
    console.log('Funcionando na porta 3000')
})




/*  IGNORE
app.get(':first/:second/:third', function (req) {
    var orderedParams = [];
    for (var i = 0; i < req.route.keys; i++) {
        orderedParams.push(req.params[req.route.keys[i].name]);
    }
    output.apply(this, orderedParams);
});

function output() {
    for(var i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
}
*/