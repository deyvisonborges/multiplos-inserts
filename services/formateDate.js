function formatDate(date) {
    var meses = [
        "Janeiro", "Fevereiro", "Marco", "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];

    var dia = new date.getDate();
    var mesIndex =new  date.getMonth();
    var ano = new date.getFullYear();

    // return dia + ' ' + meses[mesIndex] + ' ' + ano;
    return dia + mesIndex + ano;
}

module.exports = formatDate();