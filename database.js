var sqlite = require('sqlite-sync');
var fs = require('fs');
 
fs.writeFile('banco.db', 'Criei sua base de dados!', function (err) {
  if (err) throw err;
  console.log('.bd criado com sucesso!');
}); 

sqlite.connect('./banco.db');

exports.verificaInsert = (nome_campo, codigo_pessoa) => {
    let rows = sqlite.run('SELECT ' + nome_campo + ' FROM pessoa WHERE codigo_pessoa = ' + codigo_pessoa);
    return rows;
}

exports.update = async (data, codigo_pessoa) => {
    sqlite.update('pessoa', data, {ID: 1})
}

exports.insert = async(table, data) => {
    await sqlite.insert(table, data);
}
// sem tratamento de errors no momento ;)

