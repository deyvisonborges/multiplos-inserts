
## Múltiplos inserts de campos array em uma estrutura sql. 

*Crie uma base de dados SQLite*
```
CREATE TABLE pessoa (
    id integer primary key autoincrement not null,
    codigo_pessoa int not null,
    nome varchar(40) not null,
    cartoes varchar(20) not null
);

```
*Modelo de entrada de dados da API:*

```
{
   "codigo_pessoa": 15,
   "nome": "Maria",
   "cartoes": [
      "banco do brasil",	
      "americanas"
    ]
}
```

*Resultado da Consulta* <br>
![](https://i.imgur.com/34LrH0d.png) <br>

<hr>

__OBS__: Campo que possivelmente vai receber mais de um parâmetro deve ser sempre em array, caso contrário,
o laço vai adicionar caractere po caractere e vai associar ao CODIGO_PESSOA no banco.

__OBS²__: Cuidado ao utilizar o runSync do modulo do SQlite, vai dar um errinho.

Estou desevolvendo uma solução mais amigável pra todo o projeto. 
Aguarde.
